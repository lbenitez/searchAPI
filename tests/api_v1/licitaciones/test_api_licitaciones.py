from django.urls import reverse


def test_licitaciones_search_url(client):
    url = reverse("licitaciones-search")
    response = client.get(url)
    assert response.status_code == 200
