#!/usr/bin/env bash

set -e
set -x

poetry run black apps tests
poetry run isort apps tests
