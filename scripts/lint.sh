#!/usr/bin/env bash

set -e
set -x

poetry run flake8 apps tests
poetry run black apps tests --check
poetry run isort apps test --check-only