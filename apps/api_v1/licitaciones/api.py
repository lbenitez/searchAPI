from datetime import date

from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from .schemas import Licitacion, LicitacionResponse


@require_http_methods(["GET"])
def search(request):
    licitacion = Licitacion(
        id=1, id_llamado=123, fecha_llamado=date.today(), descripcion="Test Licitacion"
    )
    licitacionResponse = LicitacionResponse(data=[licitacion], page=1, total=10)
    return JsonResponse(licitacionResponse.to_dict())
