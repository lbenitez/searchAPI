from dataclasses import asdict, dataclass
from datetime import date
from typing import List

from apps.core.base import BaseResponse


@dataclass
class Licitacion:
    id: int
    id_llamado: int
    descripcion: str
    fecha_llamado: date


@dataclass
class LicitacionResponse(BaseResponse):
    data: List[Licitacion]

    def to_dict(self):
        return asdict(self)
