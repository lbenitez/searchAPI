from django.apps import AppConfig


class LicitacionesConfig(AppConfig):
    name = "apps.api_v1.licitaciones"
