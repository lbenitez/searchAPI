from django.urls import path

from . import api as licitaciones_api

urlpatterns = [path("search", licitaciones_api.search, name="licitaciones-search")]
