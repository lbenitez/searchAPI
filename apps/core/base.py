from dataclasses import dataclass


@dataclass
class BaseResponse(object):
    total: int
    page: int
